using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ADSScript : MonoBehaviour
{
    public GameObject adsCam;
    public float adsSenseDevider;
    public GameObject crosshairGameObject;
    public Sprite adsSprite;
    public Sprite notADSSprite;
    private float defaultSence;
    private FirstPersonAIO firstPersonAIO;

    // Start is called before the first frame update
    void Start()
    {
        firstPersonAIO = GetComponent<FirstPersonAIO>();
        defaultSence = firstPersonAIO.mouseSensitivity;
        crosshairGameObject.GetComponent<Image>().sprite = notADSSprite;
    }

    // Update is called once per frame
    void Update()
    {
        //print(firstPersonAIO.mouseSensitivity);

        if (Input.GetButton("HunterADS"))
        {
            if(crosshairGameObject.GetComponent<Image>().sprite != adsSprite)
            {
                crosshairGameObject.GetComponent<Image>().sprite = adsSprite;
            }

            adsCam.SetActive(true);
            firstPersonAIO.mouseSensitivity = defaultSence / adsSenseDevider;
        }
        else
        {
            if (crosshairGameObject.GetComponent<Image>().sprite != notADSSprite)
            {
                crosshairGameObject.GetComponent<Image>().sprite = notADSSprite;
            }

            adsCam.SetActive(false);
            firstPersonAIO.mouseSensitivity = defaultSence;
        }
    }
}
