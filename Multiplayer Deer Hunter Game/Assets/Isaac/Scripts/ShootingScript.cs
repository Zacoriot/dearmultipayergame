using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingScript : MonoBehaviour
{
    public Transform shotPoint;
    public GameObject hunterProjectile;

    public int maxAmmo;
    public int currentAmmo;

    public float startTimeBtwShots;
    private float timeBtwShots;

    // Start is called before the first frame update
    void Start()
    {
        currentAmmo = maxAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeBtwShots <= 0)
        {
            if (Input.GetButtonDown("HunterShoot"))
            {
                currentAmmo--;
                Instantiate(hunterProjectile, shotPoint.position, shotPoint.rotation);
                timeBtwShots = startTimeBtwShots;
            }

            if (currentAmmo == 0)
            {
                currentAmmo = maxAmmo;
            }
        }
        else
        {
            timeBtwShots -= Time.deltaTime;
        }

        
    }
}
