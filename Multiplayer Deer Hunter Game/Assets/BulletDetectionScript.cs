using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDetectionScript : MonoBehaviour
{
    public string hitPrint;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Bullet")
        {
            print(hitPrint);
        }
    }
}
