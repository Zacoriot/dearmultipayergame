using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterInteractionScript : MonoBehaviour
{
    private FirstPersonAIO FirstPersonAIO;
    private float defaultWalkSpeed;
    private float defaultRunSpeed;
    public float walkInWaterSpeedDevider;
    public float runInWaterSpeedDevider;

    // Start is called before the first frame update
    void Start()
    {
        FirstPersonAIO = GetComponent<FirstPersonAIO>();
        defaultWalkSpeed = FirstPersonAIO.walkSpeed;
        defaultRunSpeed = FirstPersonAIO.sprintSpeed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Water")
        {
            if(FirstPersonAIO.walkSpeed == defaultWalkSpeed && FirstPersonAIO.sprintSpeed == defaultRunSpeed)
            {
                FirstPersonAIO.walkSpeed = FirstPersonAIO.walkSpeed / walkInWaterSpeedDevider;
                FirstPersonAIO.sprintSpeed = FirstPersonAIO.sprintSpeed / runInWaterSpeedDevider;
                //FirstPersonAIO.canJump = false;
            }
        }
        else
        {
            FirstPersonAIO.walkSpeed = defaultWalkSpeed;
            FirstPersonAIO.sprintSpeed = defaultRunSpeed;
            //FirstPersonAIO.canJump = true;
        }
    }
}
